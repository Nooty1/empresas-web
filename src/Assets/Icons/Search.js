import React from "react";

function SearchIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="60"
      height="60"
      viewBox="0 0 60 60"
    >
      <g
        fill="none"
        fillRule="evenodd"
        stroke="#FFF"
        strokeWidth="4.148"
        transform="translate(14 13)"
      >
        <circle cx="13.378" cy="13.378" r="11.304" />
        <path strokeLinecap="round" d="M21.867 21.867l9.776 9.776" />
      </g>
    </svg>
  );
}

export default SearchIcon;
