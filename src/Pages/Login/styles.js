import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

export const BoxContainer = styled.div`
  width: fit-content;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 576px) {
    width: 100vw;
  }
`;

export const Title = styled.h3`
  font-size: 1.5rem;
  font-weight: bold;
  display: flex;
  text-align: center;
  flex-wrap: wrap;
  line-height: normal;
  width: 11rem;
  margin-bottom: 1.25rem;
`;

export const Description = styled.h4`
  font-size: 1.125rem;
  line-height: 1.625rem;
  letter-spacing: -0.25px;
  margin-bottom: 2.9rem;
  width: 22rem;
  text-align: center;
  font-size: 1.125rem;
  font-weight: 600;

  @media (max-width: 576px) {
    width: 100%;
  }
`;

export const Image = styled.img`
  width: 285px;
  margin-bottom: 4.5rem;
`;
