import {
  Container,
  Title,
  Description,
  BoxContainer,
  Image,
} from "./styles.js";
import LoginForm from "../../Components/Forms/LoginForm";
import Logo from "../../Assets/Logo/logo-home@3x.png";

function Login() {
  return (
    <Container>
      <BoxContainer>
        <Image src={Logo} alt="Logo" />
        <Title>BEM-VINDO AO EMPRESAS</Title>
        <Description>
          Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </Description>
        <LoginForm />
      </BoxContainer>
    </Container>
  );
}

export default Login;
