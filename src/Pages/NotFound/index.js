import React from "react";
import { Link } from "react-router-dom";

import { Container, Image, Title } from "./styles";

function NotFound() {
  return (
    <Container>
      <Image
        src="https://www.vippng.com/png/full/375-3750100_404-404-png.png"
        alt=""
      ></Image>
      <Title>
        Desculpe, essa página não existe, deseja fazer <Link to="/">Login</Link>
        ?
      </Title>
    </Container>
  );
}

export default NotFound;
