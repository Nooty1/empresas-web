import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

export const Image = styled.img`
  width: 40vw;
  margin-bottom: 3rem;
`;

export const Title = styled.h4`
  font-size: 1.5rem;
`;
