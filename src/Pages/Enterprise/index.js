import { useState } from "react";

import Header from "../../Components/Header";
import NavEnterprise from "../../Components/NavEnterprise";
import EnterpriseInfo from "../../Components/EnterpriseInfo";
import { Container } from "./styles";
import { useEffect, useCallback } from "react";
import EnterpriseService from "../../Service/Enterprise/EnterpriseService";

function Enterprise(props) {
  const [enterprise, setEnterprise] = useState({});

  const getEnterprise = useCallback(async () => {
    const response = await EnterpriseService.ShowEnterprise(
      props.match.params.id
    );
    setEnterprise(response.data.enterprise);
  }, [props.match.params.id]);

  useEffect(() => {
    getEnterprise();
  }, [getEnterprise]);

  return (
    <Container>
      <Header>
        <NavEnterprise title={enterprise["enterprise_name"]} />
      </Header>
      <EnterpriseInfo enterprise={enterprise} />
    </Container>
  );
}

export default Enterprise;
