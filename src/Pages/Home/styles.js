import styled from "styled-components";

export const Container = styled.div``;

export const BodyContainer = styled.div`
  min-height: calc(100vh - 9.5rem);
  display: flex;
  align-items: ${({ show }) => (show ? "flex-start" : "center")};
  justify-content: ${({ show }) => (show ? "flex-start" : "center")};
`;

export const SearchMessage = styled.h2`
  display: ${({ show }) => (show ? "none" : "flex")};
  font-size: 2rem;

  @media (max-width: 576px) {
    font-size: clamp(20px, 5vw, 2rem);
  }
`;

export const ListContainer = styled.div`
  display: ${({ show }) => (show ? "none" : "flex")};
  width: 100%;
`;
