import {
  Container,
  BodyContainer,
  SearchMessage,
  ListContainer,
} from "./styles";
import Header from "../../Components/Header";
import EnterpriseList from "../../Components/List";
import Card from "../../Components/Card";
import NavHome from "../../Components/NavHome";
import { useEffect, useState } from "react";
import EnterpriseService from "../../Service/Enterprise/EnterpriseService";

function Home() {
  const [show, setShow] = useState(false);
  const [list, setList] = useState([]);

  useEffect(() => {
    getEnterprise();
  }, []);

  const getEnterprise = async () => {
    const response = await EnterpriseService.AllEnterprise();
    setList(response.data.enterprises);
  };

  return (
    <Container>
      <Header changeHeader={show} setChangeHeader={setShow}>
        <NavHome
          changeHeader={show}
          setChangeHeader={setShow}
          setList={setList}
        />
      </Header>
      <BodyContainer show={show}>
        <SearchMessage show={show}>Clique na busca para iniciar.</SearchMessage>
        <ListContainer show={!show}>
          <EnterpriseList>
            {list.map((enterprise, enterpriseIndex) => {
              return <Card key={enterpriseIndex} enterprise={enterprise} />;
            })}
          </EnterpriseList>
        </ListContainer>
      </BodyContainer>
    </Container>
  );
}

export default Home;
