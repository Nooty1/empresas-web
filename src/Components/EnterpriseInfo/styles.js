import styled from "styled-components";

export const Container = styled.div`
  background: var(--white);
  min-height: calc(100vh - 15.5rem);
  height: 100%;
  margin: 3rem;
  border-radius: 4px;

  @media (max-width: 650px) {
    margin: 3rem 1.5rem;
  }
`;

export const BoxContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3rem 4.5rem;

  @media (max-width: 650px) {
    padding: 3rem 1.5rem;
  }
`;

export const Image = styled.img`
  @media (max-width: 650px) {
    width: 100%;
  }
`;

export const Description = styled.h3`
  margin-top: 2.5rem;
  font-size: 2.125rem;
  color: var(--warm-grey);
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;

  @media (max-width: 650px) {
    font-size: clamp(21px, 5vw, 30px);
  }
`;
