import { Container, Image, Description, BoxContainer } from "./styles";
import ImageTitle from "../ImageTitle";

function EnterpriseInfo({ enterprise }) {
  return (
    <Container>
      <BoxContainer>
        {enterprise && enterprise?.photo ? (
          <Image
            src={`https://empresas.ioasys.com.br${enterprise.photo}`}
            alt={enterprise["enterprise_name"]}
          />
        ) : (
          <ImageTitle
            title={enterprise ? enterprise["enterprise_name"] : null}
            width="100%"
            height="19rem"
          />
        )}
        <Description>{enterprise?.description}</Description>
      </BoxContainer>
    </Container>
  );
}

export default EnterpriseInfo;
