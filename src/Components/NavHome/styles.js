import styled from "styled-components";

export const Image = styled.img`
  width: 15rem;
  display: ${({ show }) => (show ? "initial" : "none")};

  @media (max-width: 576px) {
    width: 10rem;
  }
`;

export const SearchBar = styled.div`
  display: ${({ show }) => (show ? "flex" : "none")};
  border-bottom: 1px solid var(--white);
  width: 100%;
  margin-bottom: 1.4rem;
`;

export const SearchInput = styled.input`
  width: 100%;
  background: transparent;
  font-size: 2.5rem;
  color: white;
  border: none;

  ::placeholder {
    color: #991237;
  }

  @media (max-width: 576px) {
    font-size: 1.8rem;
  }
`;

export const AbsoluteIcon = styled.a`
  position: absolute;
  right: 5%;
  display: ${({ show }) => (show ? "initial" : "none")};

  @media (max-width: 576px) {
    svg {
      width: 45px;
      height: 45px;
    }
  }
`;

export const RelativeIcon = styled.a``;
