import SearchIcon from "../../Assets/Icons/Search";
import Close from "../../Assets/Icons/Close";
import NavLogo from "../../Assets/Logo/logo-nav@2x.png";

import {
  Image,
  AbsoluteIcon,
  SearchBar,
  SearchInput,
  RelativeIcon,
} from "./styles";
import EnterpriseService from "../../Service/Enterprise/EnterpriseService";

function NavHome({ changeHeader, setChangeHeader, setList }) {
  const handleChange = async (e) => {
    const response = await EnterpriseService.SearchEnterprise(e.target.value);
    setList(response.data.enterprises);
  };

  return (
    <>
      <Image show={!changeHeader} src={NavLogo} alt="NavLogo" />
      <AbsoluteIcon
        onClick={() => setChangeHeader(!changeHeader)}
        show={!changeHeader}
      >
        <SearchIcon />
      </AbsoluteIcon>
      <SearchBar show={changeHeader}>
        <SearchIcon />
        <SearchInput placeholder="Pesquisar" onChange={handleChange} />
        <RelativeIcon onClick={() => setChangeHeader(!changeHeader)}>
          <Close />
        </RelativeIcon>
      </SearchBar>
    </>
  );
}

export default NavHome;
