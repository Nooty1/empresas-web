import React from "react";
import { Link } from "react-router-dom";

import ImageTitle from "../ImageTitle";

import {
  BoxContainer,
  Image,
  TextContainer,
  Title,
  BusinessType,
  Country,
} from "./styles";

function Card({ enterprise }) {
  return (
    <Link
      style={{ textDecoration: "none", color: "var(--grey)" }}
      to={`/enterprise/${enterprise.id}`}
    >
      <BoxContainer>
        {enterprise.photo ? (
          <Image
            src={`https://empresas.ioasys.com.br${enterprise.photo}`}
            alt={enterprise["enterprise_name"]}
          />
        ) : (
          <ImageTitle
            title={enterprise["enterprise_name"]}
            width="18rem"
            height="10rem"
          />
        )}
        <TextContainer>
          <Title>{enterprise["enterprise_name"]}</Title>
          <BusinessType>
            {enterprise["enterprise_type"]["enterprise_type_name"]}
          </BusinessType>
          <Country>
            {enterprise.country} - {enterprise.city}{" "}
          </Country>
        </TextContainer>
      </BoxContainer>
    </Link>
  );
}

export default Card;
