import styled from "styled-components";

export const BoxContainer = styled.div`
  background: var(--white);
  padding: 1.7rem 1.9rem;
  margin: 1.7rem 0px;
  display: flex;
  border-radius: 4px;

  @media (max-width: 650px) {
    flex-direction: column;
  }
`;

export const Image = styled.img`
  width: 18rem;
  height: 10rem;
  object-fit: cover;
  object-position: center;

  @media (max-width: 650px) {
    width: 100%;
    height: auto;
  }
`;

export const TextContainer = styled.div`
  margin-left: 2.5rem;

  @media (max-width: 650px) {
    margin-left: 0px;
  }
`;

export const Title = styled.h2`
  font-size: 1.875rem;
  font-weight: bold;

  @media (max-width: 576px) {
    font-size: clamp(22px, 5vw, 30px);
  }
`;

export const BusinessType = styled.h3`
  font-size: 1.5rem;
  color: var(--warm-grey);

  @media (max-width: 576px) {
    font-size: clamp(20px, 4.5vw, 28px);
  }
`;

export const Country = styled.h4`
  font-size: 1.125rem;
  color: var(--warm-grey);
  @media (max-width: 576px) {
    font-size: clamp(18px, 4vw, 26px);
  }
`;
