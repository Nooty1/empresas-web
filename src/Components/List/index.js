import { Container, WarningText } from "./styles";

function List({ children }) {
  return (
    <Container>
      {children.length > 0 ? (
        children
      ) : (
        <WarningText>
          Nenhuma empresa foi encontrada para a busca realizada.
        </WarningText>
      )}
    </Container>
  );
}

export default List;
