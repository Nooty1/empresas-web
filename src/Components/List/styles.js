import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  padding: 0px 3rem;

  @media (max-width: 576px) {
    padding: 0px 1.5rem;
  }
`;

export const WarningText = styled.h3`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: calc(100vh - 9.5rem);
  font-size: 2.125rem;
  color: var(--greyish);
`;
