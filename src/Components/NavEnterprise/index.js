import Arrow from "../../Assets/Icons/Arrow";
import { useHistory } from "react-router-dom";
import { Container, Title, GoBackArrow, BoxContainer } from "./styles";

function NavEnterprise({ title }) {
  const history = useHistory();

  return (
    <Container>
      <BoxContainer>
        <GoBackArrow onClick={() => history.goBack()}>
          <Arrow />
        </GoBackArrow>
        <Title>{title}</Title>
      </BoxContainer>
    </Container>
  );
}

export default NavEnterprise;
