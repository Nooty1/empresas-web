import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  align-items: flex-end;
  padding: 2rem 0px;
`;

export const BoxContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const GoBackArrow = styled.a`
  margin-right: 5rem;
  svg {
    width: 50px;
    height: 50px;
  }
`;

export const Title = styled.h4`
  color: var(--white);
  font-size: 2.125rem;
  @media (max-width: 650px) {
    font-size: clamp(24px, 5vw, 30px);
  }
`;
