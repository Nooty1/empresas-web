import React from "react";

import { HeaderContainer } from "./styles";

function Header({ changeHeader, children }) {
  return <HeaderContainer show={changeHeader}>{children}</HeaderContainer>;
}

export default Header;
