import styled from "styled-components";

export const HeaderContainer = styled.div`
  display: flex;
  padding: 0px 3rem;
  height: 9.5rem;
  justify-content: center;
  background: var(--pink);
  background-image: linear-gradient(rgba(0, 0, 0, 0), var(--night-blue));
  align-items: ${({ show }) => (show ? "flex-end" : "center")};

  a {
    cursor: pointer;
  }

  @media (max-width: 576px) {
    padding: 0 1.5rem;
  }
`;
