import styled from "styled-components";

export const BoxTitle = styled.div`
  width: ${({ width }) => width};
  height: ${({ height }) => height};
  background: #7dc075;
  font-size: 5rem;
  color: var(--white);
  display: flex;
  align-items: center;
  justify-content: center;
`;
