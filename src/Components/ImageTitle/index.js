import React from "react";

import { BoxTitle } from "./styles";

function ImageTitle({ title, width, height }) {
  const setImageTitle = (title) => {
    if (title) {
      const newTitle = title.split(" ").map((word) => {
        return word.split("")[0];
      });
      return newTitle.join("");
    }
    return "Empresa";
  };

  return (
    <BoxTitle width={width} height={height}>
      {setImageTitle(title)}
    </BoxTitle>
  );
}

export default ImageTitle;
