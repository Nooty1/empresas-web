import { useState } from "react";
import { useHistory } from "react-router-dom";

import {
  WrapInput,
  Input,
  Button,
  Form,
  Password,
  Error,
  Loading,
} from "./styles.js";
import Email from "../../../Assets/Icons/Email";
import Lock from "../../../Assets/Icons/Lock";
import Hide from "../../../Assets/Icons/Hide";
import Show from "../../../Assets/Icons/Show";
import Lottie from "react-lottie";
import animationData from "../../../Assets/Animations/loading-spinner.json";
import LoginService from "../../../Service/Login/LoginService.js";
import { login } from "../../../Service/Auth";

function Login() {
  const [show, setShow] = useState(false);
  const [loginData, setLoginData] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const history = useHistory();

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  const handleChange = (e) => {
    let data = loginData;
    data[e.target.name] = e.target.value;
    setLoginData(data);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const { error, response } = await LoginService.LoginRequest(loginData);
    if (error) {
      setError(error);
      return setLoading(false);
    }
    if (response) {
      login(response.headers);
      return history.push("/home");
    }
  };

  return (
    <>
      <Loading loading={String(loading)}>
        <Lottie
          disabled={true}
          options={defaultOptions}
          height={400}
          width={400}
          style={{ cursor: "default" }}
          isClickToPauseDisabled={true}
        />
      </Loading>
      <Form onSubmit={handleSubmit}>
        <WrapInput>
          <Email />
          <Input
            className="email-input"
            onChange={handleChange}
            name="email"
            placeholder="E-mail"
          />
        </WrapInput>
        <WrapInput>
          <Lock />
          <Input
            className="password-input"
            onChange={handleChange}
            name="password"
            placeholder="Senha"
            type={show ? "text" : "password"}
          />
          <Password onClick={() => setShow(!show)}>
            <Hide show={!show} />
          </Password>
          <Password onClick={() => setShow(!show)}>
            <Show show={show} />
          </Password>
        </WrapInput>
        <Error error={error}>{error}</Error>
        <Button>ENTRAR</Button>
      </Form>
    </>
  );
}

export default Login;
