import styled from "styled-components";

export const Form = styled.form`
  width: 40vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;

  @media (max-width: 576px) {
    width: 100%;
  }
`;

export const WrapInput = styled.div`
  border-bottom: 1px solid var(--grey);
  margin-bottom: 2rem;
  display: flex;
  width: 360px;

  .email-input {
    margin-right: 28px;
  }

  svg {
    cursor: pointer;
  }

  @media (max-width: 576px) {
    width: 90%;
    .email-input {
      margin-right: 20px;
    }
    .password-input {
      margin-right: -8px;
    }
  }
`;

export const Input = styled.input`
  background: transparent;
  width: 100%;
  font-size: 1.125rem;
  margin-left: 1rem;
  border: none;

  ::placeholder {
    font-size: 1.125rem;
    color: var(--grey);
    opacity: 0.5;
  }

  @media (max-width: 576px) {
    margin-left: 10px;
  }
`;

export const Password = styled.a``;

export const Error = styled.p`
  margin-top: -16px;
  display: ${({ error }) => (error ? "initial" : "none")};
  position: relative;
  color: var(--pink);
  letter-spacing: 0.17px;
  font-weight: 500;
  font-size: 0.76rem;
  text-align: center;
`;

export const Button = styled.button`
  width: 20rem;
  background: var(--aquamarine);
  color: var(--white);
  margin-top: 1rem;
  font-size: 1.125rem;
  padding: 0.9rem 0px;
  border-radius: 4px;
  font-weight: bold;
  cursor: pointer;
  border: none;

  @media (max-width: 576px) {
    width: 18rem;
  }
`;

export const Loading = styled.div`
  display: ${({ loading }) => (loading === "true" ? "flex" : "none")};
  align-items: center;
  position: absolute;
  z-index: 1;
  background: rgba(255, 255, 255, 0.6);
  width: 100vw;
  height: 100vh;
  left: 0;
  top: 0;

  button {
    cursor: default;
  }
`;
