import { getAuth } from "./Auth";
import http from "./HttpService";

const CustomMessage = {
  error: "Desculpe algum erro aconteceu",
  unauthorized: "Credenciais informadas são inválidas, tente novamente.",
};

const handleResponse = (response) => {
  if (!response) {
    return { response: null, error: CustomMessage.error };
  }
  switch (response.status) {
    case 200:
      return { response, error: null };
    case 201:
      return { response, error: null };
    case 400:
      return { response: null, error: CustomMessage.error };
    case 401:
      return { response: null, error: CustomMessage.unauthorized };
    case 500:
      return { response: null, error: CustomMessage.error };
    default:
      return { response, error: CustomMessage.error };
  }
};

const getHeaders = () => {
  const auth = getAuth();
  let headers = {};
  headers = {
    headers: {
      "Content-Type": "application/json",
      "access-token": auth.token || null,
      client: auth.client || null,
      uid: auth.uid || null,
    },
  };
  return headers;
};

const getAsync = async (url) => {
  try {
    const headers = getHeaders();
    const response = await http.get(url, headers);
    return handleResponse(response);
  } catch (err) {
    return handleResponse(err.response);
  }
};

const postAsync = async (url, body) => {
  try {
    const headers = getHeaders();
    const response = await http.post(url, body, headers);
    return handleResponse(response);
  } catch (err) {
    // pass
    return handleResponse(err.response);
  }
};

const Services = { getAsync, postAsync };

export default Services;
