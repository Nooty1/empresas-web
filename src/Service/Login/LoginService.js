import BaseService from "../BaseService";

const LoginRequest = async (body) => {
  const url = "/users/auth/sign_in";
  const { response, error } = await BaseService.postAsync(url, body);
  return { response, error };
};

const LoginService = { LoginRequest };

export default LoginService;
