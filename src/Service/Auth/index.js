const Token = "auth-token";
const Client = "auth-client";
const Uid = "auth-uid";

export const isAuthenticated = () => {
  return localStorage.getItem(Token) !== null;
};

export const login = (auth) => {
  localStorage.setItem(Token, auth["access-token"]);
  localStorage.setItem(Client, auth.client);
  localStorage.setItem(Uid, auth.uid);
};

export const logout = () => {
  localStorage.removeItem(Token);
  localStorage.removeItem(Client);
  localStorage.removeItem(Uid);
};

export const getAuth = () => {
  let dataAuth = {
    token: localStorage.getItem(Token),
    client: localStorage.getItem(Client),
    uid: localStorage.getItem(Uid),
  };
  return dataAuth;
};
