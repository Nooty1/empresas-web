import BaseService from "../BaseService";

const AllEnterprise = async () => {
  const url = `/enterprises`;
  const { response } = await BaseService.getAsync(url);
  return response;
};

const SearchEnterprise = async (search) => {
  const url = `/enterprises?name=${search}`;
  const { response } = await BaseService.getAsync(url);
  return response;
};

const ShowEnterprise = async (id) => {
  const url = `/enterprises/${id}`;
  const { response } = await BaseService.getAsync(url);
  return response;
};

const EnterpriseService = { SearchEnterprise, ShowEnterprise, AllEnterprise };

export default EnterpriseService;
