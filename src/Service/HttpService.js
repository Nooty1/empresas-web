import axios from "axios";

const http = axios.create({
  baseURL: "https://empresas.ioasys.com.br/api/v1",
  timeout: 5000,
  headers: { "Content-Type": "application/json" },
});

export default http;
