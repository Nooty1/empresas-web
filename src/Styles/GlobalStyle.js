import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
* {
  margin: 0;
  padding: 0;
  outline: 0;
  box-sizing: border-box;
  font-family: 'Roboto', sans-serif;
}

html, body, #root {
  height: 100%;
  width: 100%;
  -webkit-font-smoothing: antialiased !important;
  background: var(--background-color);
  color: var(--grey);
  --background-color: #eeecdb;
  --pink: #ef5781;
  --grey: #383743;
  --light-grey: #aab1aa;
  --white: #fff;
  --aquamarine: #57bbbc;
  --night-blue: rgba(0, 0, 0, 0.2);
  --warm-grey: #8d8c8c;
  --greyish: #b5b4b4;
}
`;

export default GlobalStyle;
