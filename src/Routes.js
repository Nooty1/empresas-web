import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Enterprise from "./Pages/Enterprise";
import Home from "./Pages/Home";
import Login from "./Pages/Login";
import NotFound from "./Pages/NotFound";
import { isAuthenticated, logout } from "./Service/Auth";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      )
    }
  />
);

const Logout = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      logout();
      return (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      );
    }}
  />
);

function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <PrivateRoute path="/home" component={Home} />
        <PrivateRoute path="/enterprise/:id" component={Enterprise} />
        <Logout path="/logout" />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
