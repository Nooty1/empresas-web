import React from "react";
import GlobalStyle from "./Styles/GlobalStyle";
import Routes from "./Routes";

function App() {
  return (
    <React.Fragment>
      <GlobalStyle />
      <Routes />
    </React.Fragment>
  );
}

export default App;
